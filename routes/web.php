<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'homePageController@index');
Route::get('home', 'homePageController@index');
Route::get('/team', 'teamPageController@viewTeamSchedule');
Route::get('/teamSchedule', 'teamPageController@viewTeamSchedule');
Route::get('/dailyCoverage', 'dailyCoverageController@index');
Route::post('/coverageDay', 'dailyCoverageController@newDay');
Route::get('/admin', 'adminPageController@index');
Route::post('/update', 'updateController@index');
Route::post('/addDay', 'updateController@addDay');
Route::post('/addRegularDay', 'updateController@addRegularDay');
Route::post('/addSpecificDay', 'updateController@addSpecificDay');
Route::post('/deleteRegularScheduleDay', 'updateController@deleteRegularScheduleDay');
Route::post('/deleteSpecificDay', 'updateController@deleteSpecificDay');
Route::post('/deleteDate', 'updateController@deleteDate');
Route::post('/deleteUser', 'updateController@deleteUser');
Route::get('/logout', 'homePageController@logout');