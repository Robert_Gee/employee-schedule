<?php

namespace App\libraries;

DEFINE('hsl_URL', 'https://hsapp.hs.umt.edu/login/index.php');

class Login {
	//Initial Config
	private $splash;
	private $namespace;
	private $ticket;
	private $address;

	//Downloaded Data
	private $uuid;
	private $nameFirst;
	private $nameLast;
	private $spaces;

	public function __construct($data) {
		//Make sure session library has been loaded
		if(session_status() == PHP_SESSION_NONE) {
			session_start();
		}

		//Set class attributes
		$this->splash = $data['splash'];
		$this->namespace = $data['namespace'];
		$this->address = urlencode($_SERVER["REMOTE_ADDR"]);

		//setup ticket and last-update-time
		if(isset($_GET['hsl_ticket'])) {
			$this->ticket = $_GET['hsl_ticket'];
			$time = 0;
		} elseif(isset($_SESSION['hsl_ticket'])){
			$this->ticket = $_SESSION['hsl_ticket'];
			$time = $_SESSION['hsl_time'];
		} else {
			$this->ticket = '';
			$time = 0;
		}
		//download from server
		if($this->ticket) {
			$data = $this->api('permissions', implode(':', $this->namespace), array($time));
			if($data == false) {
				unset($_SESSION['hsl_ticket']);
				unset($_SESSION['hsl_time']);
				unset($_SESSION['hsl_uuid']);
				unset($_SESSION['hsl_nameFirst']);
				unset($_SESSION['hsl_nameLast']);
				unset($_SESSION['hsl_spaces']);
				return;
			}

			//save ticket and last-update-time for later use
			$_SESSION['hsl_ticket'] = $this->ticket;
			$_SESSION['hsl_time'] = $data->time;

			if($time == 0) {
				//Loading for the first time
				$this->uuid = $data->uuid;
				$this->nameFirst = $data->nameFirst;
				$this->nameLast = $data->nameLast;
				$this->spaces = $data->spaces;
			} else {
				//Loading from session, syncing any changes
				$this->uuid = $_SESSION['hsl_uuid'];
				$this->nameFirst = $_SESSION['hsl_nameFirst'];
				$this->nameLast = $_SESSION['hsl_nameLast'];
				$this->spaces = $_SESSION['hsl_spaces'];
				foreach($data->spaces as $name=>$obj) {
					$this->spaces->$name = $obj;
				}
			}

			//save data into session
			$_SESSION['hsl_uuid'] = $this->uuid;
			$_SESSION['hsl_nameFirst'] = $this->nameFirst;
			$_SESSION['hsl_nameLast'] = $this->nameLast;
			$_SESSION['hsl_spaces'] = $this->spaces;
		}
	}

	private function load_url($url) {
		$raw = file_get_contents(hsl_URL . $url, false);
		if($raw == FALSE) {
			//show_error('Failed to read url', 500);
			return false;
		}

		$data = json_decode($raw);
		if($data == NULL) {
			//show_error('Failed to decode json', 500);
			return false;
		}

		return $data;
	}

	public function api($method, $namespace, $args = array()) {
		$argstr = '';
		foreach($args as $arg) {
			if($arg !== '' && $arg !== NULL) {
				$argstr .= $arg . '/';
			} else {
				$argstr .= 'NULL/';
			}
		}
		if(empty($args)) $args = '';
		else $args = implode('/', $args);
		$data = $this->load_url('/api/' . $method .'/' . $this->ticket . '/' . $namespace . '/' . $this->address . '/' . $argstr);
		return $data;
	}

	public function reload() {
		$_SESSION['hsl_time'] = 0;
	}

	public function redirect($path = "") {
		$from = 'https://hsapp.hs.umt.edu/_staging/employee-schedule/' . $path;

		header("Location: " . hsl_URL . "/main/login?hslSplash=" . $this->splash . '&hslReturn=' . urlencode($from));
		die();
	}

	public function logout() {
		session_destroy();
		header("Location: " . hsl_URL . '/main/logout/' . $this->ticket);
		die();
	}

	//is_ functions, to test permission
	public function is_authenticated() {
		if($this->uuid) return true;
		return false;
	}

	public function is_member($namespace) {
		//Redirect user if not logged int
		if(!$this->is_authenticated()) $this->redirect();

		//Load Space Data
		if(!property_exists($this->spaces, $namespace)) return false;
		return true;
	}

	public function is_a($namespace, $type) {
		//Redirect user if not logged int
		if(!$this->is_authenticated()) $this->redirect();

		//Load Space Data
		if(!property_exists($this->spaces, $namespace)) return false;
		if(!property_exists($this->spaces->$namespace->roles, $type)) return false;
		$nType = $this->spaces->$namespace->roles->$type;
		$nAuth = $this->spaces->$namespace->auth;

		//Authority Check
		$result = $nType & $nAuth;
		return $result != 0;
	}

	//get_ functions
	public function get_identity() {
		$res = new \stdCLass();
		
		$res->uuid = $this->uuid;
		$res->nameFirst = $this->nameFirst;
		$res->nameLast = $this->nameLast;
		$res->nameFull = $res->nameFirst . ' ' . $res->nameLast;

		return $res;
	}

	public function get_namespaces($parent = NULL, $role = NULL) {
		$result = array();
		foreach($this->spaces as $space=>$data) {
			if($parent) {
				if(!preg_match("/^$parent-[0-9a-zA-Z_-]*$/", $space)) {
					continue;
				}
			}
			if($role) {
				if(!$this->is_a($space, $role)) {
					continue;
				}
			}
			$result[] = $space;
		}
		return $result;
	}

	public function get_roles($namespace) {
		if(!property_exists($this->spaces, $namespace)) return NULL;
		return $this->spaces->$namespace->roles;
	}
}