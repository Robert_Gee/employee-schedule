<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\libraries\login;

class homePageController extends Controller
{
    public function index() 
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect();
        }else {
            //get the identity of the user that logged in.  
            $firstName = $login_client->get_identity()->nameFirst;
            $lastName = $login_client->get_identity()->nameLast;
           
            //get all active sfd employees from employee database 2013 api 
            $raw = file_get_contents('http://hsapp.hs.umt.edu/_staging/employee-database/2013_API/server.php/school/19/department/1/type/66,68/tag/72,127,138,528/JSON');
            
            //loop through the data until the matching first and last name are found. 
            $data = json_decode($raw);
            foreach($data as $person)
            {
                if($person->FirstName === $firstName and $person->LastName === $lastName)
                {   //when matching user is found from data get the netid
                    $netId = $person->netid;
                }
            }
            
            //insert ignore to insert user info to db if it does not already exist.               
            $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            if($user == NULL)
            {
                DB::statement("INSERT IGNORE INTO users(FirstName, LastName, netId)VALUES('$firstName', '$lastName', '$netId')");
                $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            }
            $dates = DB::select("SELECT * FROM dates WHERE netId = '$netId' ORDER BY sDate, StartTime ");

            //fetch the days of the week. 
            $days = DB::select("SELECT * FROM days");

            //fetch the regular schedule for a user
            $schedule = DB::select("SELECT * FROM shifts WHERE NetId = '$netId'");

            //add users to the new table. 
            $user_new = DB::select("SELECT * FROM users_new WHERE NameFirst = '$firstName' AND NameLast = '$lastName'");

            //fetch changes for the user
            $changes = DB::select("SELECT * FROM changes WHERE NetId = '$netId'");

            if($user_new == NULL)
            {
                DB::statement("INSERT IGNORE INTO users_new(NetId, NameFirst, NameLast)VALUES('$netId', '$firstName', '$lastName')");
            }
            
            return view('welcome', compact('user', 'dates', 'days', 'schedule', 'changes', 'user_new'));
        }
    }
    
    public function logout()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        $login_client->logout();
    }
}
