<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\libraries\login;

class teamPageController extends Controller
{
    public function index() 
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        //get all users and dates from the db. Will be sorted later. 
        $users = DB::select("SELECT * FROM users ORDER BY FirstName");
        $dates = DB::select("SELECT * FROM dates ORDER BY sDate, StartTime");

        //fetch schedule changes
        $changes = DB::select("SELECT * FROM changes");
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('team');
        }else {
            return view('team', compact('users', 'dates', 'changes'));
        }
    }

    public function viewTeamSchedule() 
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        //get all users and dates from the db. Will be sorted later. 
        $users = DB::select("SELECT * FROM users_new");

        //get all shifts
        $shifts = DB::select("SELECT * FROM shifts");

        //get days of week
        $days = DB::select("SELECT * FROM days");

        //fetch schedule changes
        $changes = DB::select("SELECT * FROM changes");
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('team');
        }else {
            return view('teamSchedule', compact('users', 'shifts', 'days', 'changes'));
        }
    }
}
