<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\libraries\login;
use Exception;

class updateController extends Controller
{
    public function index() 
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            
            //get data from from. used for sql query. 
            $firstName = request('empFName');
            $lastName = request('empLName');
            $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            $user_new = DB::select("SELECT * FROM users_new WHERE NameFirst = '$firstName' AND NameLast = '$lastName'");
            $netId = $user[0]->netId;
            $dates = DB::select("SELECT * FROM dates WHERE netId = '$netId' ORDER BY sDate, StartTime ");
            $allUsers = DB::select("SELECT * FROM users ORDER BY FirstName");
            //fetch the days of the week. 
            $days = DB::select("SELECT * FROM days");

            //fetch the regular schedule for a user
            $schedule = DB::select("SELECT * FROM shifts WHERE NetId = '$netId'");

            //fetch schedule changes
            $changes = DB::select("SELECT * FROM changes WHERE NetId = '$netId'");

            return view('updateSchedule', compact('user', 'dates', 'allUsers', 'user_new' ,'days', 'schedule', 'changes'));
        }
    }
    
    public function addDay()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            DB::table('dates')->insert(
            ['sDate' => request('date'), 'startTime' => request('sTime'), 'endTime' => request('eTime'), 'netId' => request('netId')]
            );   
            
            $firstName = request('empFName');
            $lastName = request('empLName');
            $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            $user_new = DB::select("SELECT * FROM users_new WHERE NameFirst = '$firstName' AND NameLast = '$lastName'");
            $netId = $user[0]->netId;
            $dates = DB::select("SELECT * FROM dates WHERE netId = '$netId' ORDER BY sDate, StartTime ");
            $allUsers = DB::select("SELECT * FROM users ORDER BY FirstName");

            //fetch the days of the week. 
            $days = DB::select("SELECT * FROM days");

            //fetch the regular schedule for a user
            $schedule = DB::select("SELECT * FROM shifts WHERE NetId = '$netId'");

            //fetch schedule changes
            $changes = DB::select("SELECT * FROM changes WHERE NetId = '$netId'");

            return view('updateSchedule', compact('user', 'dates', 'allUsers', 'user_new' ,'days', 'schedule', 'changes'));
        }
    }

    public function addRegularDay()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            DB::table('shifts')->insert(
            ['DayCode' => request('date'), 'Start' => request('sTime'), 'End' => request('eTime'), 'NetId' => request('netId')]
            );   
            
            $firstName = request('empFName');
            $lastName = request('empLName');
            $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            $user_new = DB::select("SELECT * FROM users_new WHERE NameFirst = '$firstName' AND NameLast = '$lastName'");
            $netId = $user[0]->netId;
            $dates = DB::select("SELECT * FROM dates WHERE netId = '$netId' ORDER BY sDate, StartTime ");
            $allUsers = DB::select("SELECT * FROM users ORDER BY FirstName");

            //fetch the days of the week. 
            $days = DB::select("SELECT * FROM days");
            //fetch the regular schedule for a user
            $schedule = DB::select("SELECT * FROM shifts WHERE NetId = '$netId'");

            //fetch schedule changes
            $changes = DB::select("SELECT * FROM changes WHERE NetId = '$netId'");

            return view('updateSchedule', compact('user', 'dates', 'allUsers', 'user_new' ,'days', 'schedule', 'changes'));;
        }
    }

    public function addSpecificDay()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            DB::table('changes')->insert(
            ['Date' => request('date'), 'Type' =>request('type'), 'Start' => request('sTime'), 'End' => request('eTime'), 'NetId' => request('netId')]
            );   
            
            $firstName = request('empFName');
            $lastName = request('empLName');
            $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            $user_new = DB::select("SELECT * FROM users_new WHERE NameFirst = '$firstName' AND NameLast = '$lastName'");
            $netId = $user[0]->netId;
            $dates = DB::select("SELECT * FROM dates WHERE netId = '$netId' ORDER BY sDate, StartTime ");
            $allUsers = DB::select("SELECT * FROM users ORDER BY FirstName");

            //fetch the days of the week. 
            $days = DB::select("SELECT * FROM days");
            //fetch the regular schedule for a user
            $schedule = DB::select("SELECT * FROM shifts WHERE NetId = '$netId'");

            //fetch schedule changes
            $changes = DB::select("SELECT * FROM changes WHERE NetId = '$netId'");

            return view('updateSchedule', compact('user', 'dates', 'allUsers', 'user_new' ,'days', 'schedule', 'changes'));
        }
    }
    
    public function deleteDate()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            DB::table('dates')->where('netId',request('netId'))->where('sDate',request('date'))->where('startTime',request('sTime'))->where('endTime', request('eTime'))->delete();  
            
            $firstName = request('empFName');
            $lastName = request('empLName');
            $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            $user_new = DB::select("SELECT * FROM users_new WHERE NameFirst = '$firstName' AND NameLast = '$lastName'");
            $netId = $user[0]->netId;
            $dates = DB::select("SELECT * FROM dates WHERE netId = '$netId' ORDER BY sDate, StartTime ");
            $allUsers = DB::select("SELECT * FROM users ORDER BY FirstName");

            //fetch the days of the week. 
            $days = DB::select("SELECT * FROM days");
            //fetch the regular schedule for a user
            $schedule = DB::select("SELECT * FROM shifts WHERE NetId = '$netId'");
            
            //fetch schedule changes
            $changes = DB::select("SELECT * FROM changes WHERE NetId = '$netId'");

            return view('updateSchedule', compact('user', 'dates', 'allUsers', 'user_new' ,'days', 'schedule', 'changes'));
        }
    }

    public function deleteRegularScheduleDay()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            DB::table('shifts')->where('netId',request('netId'))->where('dayCode', request('day'))->delete();  
            
            $firstName = $login_client->get_identity()->nameFirst;
            $lastName = $login_client->get_identity()->nameLast;
            $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            $user_new = DB::select("SELECT * FROM users_new WHERE NameFirst = '$firstName' AND NameLast = '$lastName'");
            $netId = $user[0]->netId;
            $dates = DB::select("SELECT * FROM dates WHERE netId = '$netId' ORDER BY sDate, StartTime ");
            $allUsers = DB::select("SELECT * FROM users ORDER BY FirstName");

            //fetch the days of the week. 
            $days = DB::select("SELECT * FROM days");
            //fetch the regular schedule for a user
            $schedule = DB::select("SELECT * FROM shifts WHERE NetId = '$netId'");
            
            //fetch schedule changes
            $changes = DB::select("SELECT * FROM changes WHERE NetId = '$netId'");

            return view('updateSchedule', compact('user', 'dates', 'allUsers', 'user_new' ,'days', 'schedule', 'changes'));
        }
    }

    public function deleteSpecificDay()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            DB::table('changes')->where('NetId',request('netId'))->where('Date', request('date'))->delete();  
            
            $firstName = $login_client->get_identity()->nameFirst;
            $lastName = $login_client->get_identity()->nameLast;
            $user = DB::select("SELECT * FROM users WHERE FirstName = '$firstName' AND LastName = '$lastName'");
            $user_new = DB::select("SELECT * FROM users_new WHERE NameFirst = '$firstName' AND NameLast = '$lastName'");
            $netId = $user[0]->netId;
            $dates = DB::select("SELECT * FROM dates WHERE netId = '$netId' ORDER BY sDate, StartTime ");
            $allUsers = DB::select("SELECT * FROM users ORDER BY FirstName");

            //fetch the days of the week. 
            $days = DB::select("SELECT * FROM days");
            //fetch the regular schedule for a user
            $schedule = DB::select("SELECT * FROM shifts WHERE NetId = '$netId'");
            
            //fetch schedule changes
            $changes = DB::select("SELECT * FROM changes WHERE NetId = '$netId'");

            return view('updateSchedule', compact('user', 'dates', 'allUsers', 'user_new' ,'days', 'schedule', 'changes'));
        }
    }
    
    public function deleteUser()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            
            DB::table('dates')->where('netId',request('netId'))->delete();  
            DB::table('users')->where('netId',request('netId'))->delete();
            DB::table('users_new')->where('NetId', request('netId')->delete);
            DB::table('shifts')->where('NetId', request('netId')->delete);
            DB::table('changes')->where('NetId', request('netId')->delete);
            
            //get all users and dates from the db. Will be sorted later. 
            $users = DB::select("SELECT * FROM users ORDER BY FirstName");
            $dates = DB::select("SELECT * FROM dates");
            
            if($login_client->is_a('sfd_schedule', 'admin'))
            {
                $admin = true;
            }
            else
            {
                $admin = false;
            }
            return view('admin', compact('users', 'dates', 'admin'));
        }
    }
}
