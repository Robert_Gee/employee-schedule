<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\libraries\login;

class adminPageController extends Controller
{
    public function index() 
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        //get all users and dates from the db. Will be sorted later. 
        $users = DB::select("SELECT * FROM users ORDER BY FirstName");
        $dates = DB::select("SELECT * FROM dates ORDER BY sDate, StartTime");

        //fetch shifts
        $shifts = DB::select("SELECT * FROM shifts");

        //fetch days of the week
        $days = DB::select("SELECT * FROM days");

        //fetch user info from new table 
        $users_new = DB::select("SELECT * FROM users_new");

        //fetch schedule changes for specific days. 
        $changes = DB::select("SELECT * FROM changes");
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('admin');
        }else {
            
            if($login_client->is_a('sfd_schedule', 'admin'))
            {
                $admin = true;
            }
            else
            {
                $admin = false;
            }
            return view('admin', compact('users', 'dates', 'admin', 'shifts', 'days', 'users_new', 'changes'));
        }
    }
}
