<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\libraries\login;

class dailyCoverageController extends Controller
{
    public function index() 
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        //get all users and dates from the db. Will be sorted later. 
        $users = DB::select("SELECT * FROM users ORDER BY FirstName");
        $dates = DB::select("SELECT * FROM dates ORDER BY sDate, StartTime");
        
        $selectedDate = date("D, F d", strtotime("+0 days"));
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('dailyCoverage');
        }else {
            return view('dailyCoverage', compact('users', 'dates', 'selectedDate'));
        }
    }
    
    public function newDay()
    {
        $login_config = array();
        $login_config['splash'] = "SFDSCHEDULE";
        $login_config['namespace'] = array('sfd_schedule');

        $login_client = new Login($login_config);
        
        //get all users and dates from the db. Will be sorted later. 
        $users = DB::select("SELECT * FROM users ORDER BY FirstName");
        $dates = DB::select("SELECT * FROM dates ORDER BY sDate, StartTime");
        
        $selectedDate = date("D, F d", strtotime(request('date')));
        
        if(!$login_client->is_authenticated()){
            $login_client->redirect('dailyCoverage');
        }else {
            return view('dailyCoverage', compact('users', 'dates', 'selectedDate'));
        }
    }
}
