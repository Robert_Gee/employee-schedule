<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
    <head>
        <meta charset="utf-8">

        <title>SFD Schedule</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="sfd-favicon.ico" />
    </head>
    <body>
      <div class="teamSchedule">
        @component('components.topNav')
        @endcomponent

        <div class="container-fluid" style="margin-top:10px; background-color:white;">
          <h1 align="center">Team Schedule</h1>
            
            
            <?php
                foreach($users as $user){
                    echo "<div style='border:1px solid; margin:10px;'>";
                    $raw = file_get_contents('http://hsapp.hs.umt.edu/employee-database/2013_API/server.php/school/19/department/1/type/66,68/tag/72,127,138,528/JSON');
                
                    $data = json_decode($raw);
                    foreach($data as $person)
                    {
                        if($person->FirstName === $user->FirstName and $person->LastName === $user->LastName)
                        {
                            $photoUrl = $person->Photo;
                            echo "<img src='http://hsapp.hs.umt.edu/employee-database/index.php/pubtools/serveFile/images/$photoUrl' style='max-height:75px; max-width:75px; padding:5px; border:1px' alt='Profile picture for $person->FirstName $person->LastName'";
                            echo "<br>";
                        }
                    }
                    echo "<h3>Schedule for $user->FirstName $user->LastName</h3>";
                    echo "<table>";
                    echo "<th>Date</th>";
                    echo "<th>Schedule</th>";
                    foreach($dates as $date){
                        if($user->netId == $date->netId){ //this is what sorts the users and dates. 
                        echo "<tr>";
                            echo"<td>"; echo date("D, F d", strtotime($date->sDate)) . ": "; echo"</td>";
                            echo"<td>"; echo date("g:i a",strtotime($date->startTime)) . " - " . date("g:i a", strtotime($date->endTime)) . " "; echo"</td>";
                        echo "</tr>";
                        }
                    }
                    echo "</table>";
                    echo "</div>";
                }
            ?>

          @component('components.footer')
          @endcomponent
        </div>

      </div>

    </body>
</html>
