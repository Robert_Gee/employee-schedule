<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
    <head>
        <meta charset="utf-8">

        <title>SFD Schedule</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="sfd-favicon.ico" />
    </head>
    <body>
      <div>
        <div class="container-fluid" style="background-color:white;">
            @component('components.topNav')
            @endcomponent
            <h1 align="center" >Daily Coverage</h1>
            
            <div style="overflow-x:auto;">
            <h2>Coverage for <?php echo $selectedDate ?></h2>
            <table style='border-collapse: separate; border-spacing: 1px;'>
                <tr>
                    <th>Name</th>
                    <?php
                    for($i = 0; $i < 14; $i++)
                        {
                            $sTime = date("g:i a", strtotime('07:00') + 60*60*$i);
                            echo "<th>$sTime </th>";
                        }
                    ?>
                </tr>
                    <?php
                    $color = "blue";
                    foreach($users as $user)
                    {
                        
                            foreach($dates as $date)
                            {
                                if($date->netId === $user->netId and date("D, F d", strtotime($date->sDate)) === $selectedDate)
                                {
                                    echo "<tr>";
                                    echo "<td style='width: 100px;'>$user->FirstName $user->LastName</td>";
                                    $firstTime = true;
                                    for($i = 0; $i < 14; $i++)
                                    {
                                        
                                        $currentTime = date("H:i", strtotime('07:00') + 60*60*$i);
                                        $scheduleSTime = date("H:i", strtotime($date->startTime));
                                        $scheduleETime = date("H:i", strtotime($date->endTime));
                                        if($currentTime >= $scheduleSTime and $currentTime < $scheduleETime)
                                        {
                                            if($firstTime)
                                            {
                                                $startTime = date("g:i", strtotime($date->startTime));
                                                $endTime = date("g:i", strtotime($date->endTime));
                                                if($user->FirstName == "Seth")
                                                    echo "<td style='background-color:#1164a3; color:white; width: 100px;'>$startTime-$endTime</td>";
                                                else
                                                    echo "<td style='background-color:$color; color:white; width: 100px;'>$startTime-$endTime</td>";
                                                $firstTime = false;
                                            }
                                            else
                                            {
                                                if($user->FirstName == "Seth")
                                                    echo "<td style='background-color:#1164a3; color:white; width: 100px;'></td>";
                                                else
                                                    echo "<td style='background-color:$color; color:white; width: 100px;'></td>";
                                            }
                                        }
                                        elseif($currentTime <= $scheduleSTime || $currentTime >= $scheduleETime)
                                        {
                                            echo "<td style='color:black; width: 100px;'></td>";
                                        }
                                    }
                                    echo "</tr>";
                                }
                            }
                        switch ($color) 
                        {
                            case "blue":
                                $color = "red";
                                break;
                            case "red":
                                $color = "green";
                                break;
                            case "green":
                                $color = "blue";
                        }
                    }
                    ?>
            </table>
                <h3>Select a Different Day</h3>
                <form action="coverageDay" method='post'>
                    {{csrf_field()}}
                    <select name="date" class="form-control">
                        <?php 
                        for($i = 0; $i < 32; $i++)
                        {          
                            $date = date("D, F d", strtotime("+$i days"));
                            $dateEntry = date("Y-m-d", strtotime("$date"));
                            echo "<option value='$dateEntry'>$date</option>";
                        }
                        ?>
                    </select>
                    <br>
                    <button type='submit' value='Submit' class="btn btn-success">Submit</button>
                    <br><br>
                </form>
            </div>
            
            @component('components.alert')
            @slot('type')
                danger
            @endslot
            
            @slot('title')
                Multiple shifts are shown on seperate lines. Visual representation is rounded down to the hour. 
            @endslot
          @endcomponent

          @component('components.footer')
          @endcomponent
        </div>

      </div>

    </body>
</html>
