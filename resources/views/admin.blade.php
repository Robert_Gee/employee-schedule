<!-- Stored in resources/views/layouts/app.blade.php -->
<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
    <head>
        <meta charset="utf-8">

        <title>SFD Schedule</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="sfd-favicon.ico" />
    </head>
    <body>
      <div>
        <div class="container-fluid teamSchedule" style="background-color:white;">
        @component('components.topNav')
        @endcomponent
          <h1 align="center">Admin Panel</h1>
            
          @if($admin == false)
            @component('components.alert')
            @slot('type')
                danger
            @endslot
            
            @slot('title')
                Access Denied
            @endslot
          @endcomponent
            
          @else
            <?php
                foreach($users_new as $user){
                    echo "<div style='border:1px solid; margin:10px;'>";
                    $raw = file_get_contents('http://hsapp.hs.umt.edu/employee-database/2013_API/server.php/school/19/department/1/type/66,68/tag/72,127,138,528/JSON');
                    
                    $data = json_decode($raw);
                    foreach($data as $person)
                    {
                        if($person->FirstName === $user->NameFirst and $person->LastName === $user->NameLast)
                        {
                            $photoUrl = $person->Photo;
                            echo "<img src='http://hsapp.hs.umt.edu/employee-database/index.php/pubtools/serveFile/images/$photoUrl' style='max-height:75px; max-width:75px; padding:5px; alt='Profile picture for $person->FirstName $person->LastName''";
                            echo "<br>";
                        }
                    }
                    
                    echo "<h3>Schedule for $user->NameFirst $user->NameLast</h3>"; echo " ";
                    
                    echo "<form action='update' method='post' id='$user->NameLast'>"; //id must be specific to each user
                    echo "<input type='hidden' name='empFName' value='$user->NameFirst'>";
                    echo "<input type='hidden' name='empLName' value='$user->NameLast'>";
                    echo csrf_field();
                    echo "</form>";
                    
                    echo "<button type='submit' form='$user->NameLast' value='Submit' class='btn btn-primary'>Update this Schedule</button>";
                    
                    echo "<table>";
                    echo "<th>Date</th>";
                    echo "<th>Schedule</th>";

                    $count = 0;
                    foreach($days as $day){
                                echo "<tr>";
                                $today = date("l");
                                if($today == "Monday") {
                                echo"<td>"; echo date("D, F d", strtotime("Monday +$count days")) . ": "; echo"</td>";
                                $date = date("D, F d", strtotime("Monday +$count days"));
                            }
                            else {
                                echo"<td>"; echo date("D, F d", strtotime("Last Monday +$count days")) . ": "; echo"</td>";
                                $date = date("D, F d", strtotime("Last Monday +$count days"));
                            }
                            $count += 1;
                            //to show dates
                            // echo"<td>"; echo date("D, F d", strtotime("Last Monday +$count days")) . ": "; echo"</td>";
                            $emptyDay = true;
                            $scheduleChange = false;

                            foreach($changes as $change) {
                                $changeDate = date("D, F d",strtotime($change->Date));
                                if($change->NetId == $user->NetId && $changeDate == $date) {
                                    if($scheduleChange == false) echo "<td>";
                                    else echo " | ";
                                    $scheduleChange = True;
                                    if($change->Type == 2) {
                                        echo date("g:i a",strtotime($change->Start)) . " - " . date("g:i a",strtotime($change->End)) . " ";
                                    }
                                    else {
                                        echo "Not Scheduled";
                                    }
                                    
                                }
                                if($scheduleChange == true) echo "</td>";
                            }

                            if($scheduleChange == false) {
                                $emptyDay = true;
                                foreach($shifts as $shift){
                                    if($days[$shift->DayCode]->Name == $day->Name && $shift->NetId == $user->NetId) {
                                        if($emptyDay) echo "<td>";
                                        else echo " | ";
                                        echo date("g:i a",strtotime($shift->Start)) . " - " . date("g:i a",strtotime($shift->End)) . " ";
                                        $emptyDay = false;
                                    }
                                }
                                if($emptyDay) {
                                    echo"<td>"; echo "Not Scheduled"; echo"</td>";
                                }
                                else {
                                    echo"</td>";
                                }
                            }
                            //echo"<td>"; echo date("g:i a",strtotime($date->startTime)) . " - " . date("g:i a", strtotime($date->endTime)) . " "; echo"</td>";
                        echo "</tr>";
    
                        }
                    echo "</table>";
                    echo "</div>";
                }
            ?>
            
            <h3>Add a User</h3>
            
            @component('components.alert')
            @slot('type')
                success
            @endslot
            
            @slot('title')
                To add a user simply add them to the sfd_schedule group on the <a href='http://hsapp.hs.umt.edu/login/index.php/Manage/namespace_/717' target="_blank">H&amp;S Management Panel</a> and then, have them log into this website.
            @endslot
          @endcomponent
            
            <h3>Delete a user</h3>
            
            <?php 
                    echo "<table>";
                    echo "<th>First Name</th>";
                    echo "<th>Last Name</th>";
                    echo "<th>Netid</th>";
                    echo "<th>Options</th>";
                foreach($users as $user){
                    echo "<tr>";
                        echo "<td>$user->FirstName</td>";
                        echo "<td>$user->LastName</td>";
                        echo "<td>$user->netId</td>";
                        echo"<form action='deleteUser' method='post' id='$user->FirstName' onsubmit='return delete_confirm()'>";
                        echo csrf_field();
                        echo "<input type='hidden' name='FirstName' value='$user->FirstName'>";
                        echo "<input type='hidden' name='LastName' value='$user->LastName'>";
                        echo "<input type='hidden' name='netId' value='$user->netId'>";
                        echo"<td><button type='submit' value='Submit' class='btn btn-danger'>Delete this User</button></td>";
                        echo"</form>";
                    echo "</tr>";
                }
                    echo "</table>";
                    echo "<br>";
            
                echo "<script> function delete_confirm(){return confirm('You are about to delete $user->FirstName $user->LastName')}</script>";
            
            ?>
            
            @component('components.alert')
            @slot('type')
                danger
            @endslot
            
            @slot('title')
                Users must also be removed from the sfd_schedule group on the <a href='http://hsapp.hs.umt.edu/login/index.php/Manage/namespace_/717' target="_blank">H&amp;S Management Panel</a>
            @endslot
          @endcomponent
          @endif

          @component('components.footer')
          @endcomponent

        </div>

      </div>

    </body>
</html>
