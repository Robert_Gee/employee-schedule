<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
    <head>
        <meta charset="utf-8">

        <title>SFD Schedule</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="sfd-favicon.ico" />
    </head>
    <body>
      <div>
        <div class="container-fluid" style="background-color:white;">
        @component('components.topNav')
        @endcomponent
          <h1 align="center">Updating a Schedule</h1>
            
            <div class="container-fluid" style="margin-top:10px; background-color:white;">
            
            <div style="border:1px solid; margin 10px">
            <?php 
                $raw = file_get_contents('http://hsapp.hs.umt.edu/employee-database/2013_API/server.php/school/19/department/1/type/66,68/tag/72,127,138,528/JSON');
                
                $data = json_decode($raw);
                foreach($data as $person)
                {
                    if($person->FirstName === $user[0]->FirstName and $person->LastName === $user[0]->LastName)
                    {
                        $photoUrl = $person->Photo;
                        echo "<img src='http://hsapp.hs.umt.edu/employee-database/index.php/pubtools/serveFile/images/$photoUrl' style='max-height:75px; max-width:75px; padding:5px;' alt='Profile picture for $person->FirstName $person->LastName'";
                        echo "<br>";
                    }
                }
            
            ?>
            
            <h3>Schedule for <?php echo $user[0]->FirstName . " " . $user[0]->LastName; ?></h3>
            
            <table>
                <tr>
                    <th>Day</th>
                    <th>Schedule</th>
                    <th>Options</th>
                </tr>
                <?php 
                    if($days == NULL)
                    {
                        echo"<td>Nothing to show</td>";
                        echo"<td>Nothing to show</td>";
                    }
                    else
                    {
                        foreach($days as $day){
                            echo "<tr>";
                            echo"<td>"; echo $day->Name; echo"</td>";
                            $emptyDay = true;
                            foreach($schedule as $shift){
                                if($days[$shift->DayCode]->Name == $day->Name) {
                                    if($emptyDay) echo "<td>";
                                    else echo " | ";
                                    echo date("g:i a",strtotime($shift->Start)) . " - " . date("g:i a",strtotime($shift->End)) . " ";
                                    $emptyDay = false;
                                }
                            }
                            if($emptyDay) {
                                echo"<td>"; echo "Not Scheduled"; echo"</td>";
                            }
                            else {
                                echo"</td>";
                            }

                            $netId = $user_new[0]->NetId;

                            
                            echo"<form action='deleteRegularScheduleDay' method='post' id='$day->Name'>";
                            echo csrf_field();
                            echo "<input type='hidden' name='netId' value='$netId'>";
                            echo "<input type='hidden' name='day' value='$day->Id'>";
                            echo"<td><button type='submit' value='Submit' class='btn btn-danger'>Delete this day</button></td>";
                            echo"</form>";
                            //echo"<td>"; echo date("g:i a",strtotime($date->startTime)) . " - " . date("g:i a", strtotime($date->endTime)) . " "; echo"</td>";
                            echo "</tr>";
    
                        }
                    }
    
                    ?>
            </table>
            </div>
            <br>
            
            <div style="border:1px solid; margin 10px"> 
            <h3>Schedule changes for specific days</h3>
            
            <table>
                <tr>
                    <th>Day</th>
                    <th>Schedule</th>
                    <th>Options</th>
                </tr>
                <?php 
                    if($days == NULL)
                    {
                        echo"<td>Nothing to show</td>";
                        echo"<td>Nothing to show</td>";
                    }
                    else
                    {
                        foreach($changes as $change){
                            echo "<tr>";
                            echo"<td>"; echo date("D, F d", strtotime($change->Date)); echo"</td>";
                            if($change->NetId == $user_new[0]->NetId) {
                                echo "<td>";
                                if($change->Type == 2) {
                                    echo date("g:i a",strtotime($change->Start)) . " - " . date("g:i a",strtotime($change->End)) . " ";
                                }
                                else {
                                    echo "Not Scheduled";
                                }
                                echo "</td>";
                            }

                            $netId = $user_new[0]->NetId;

                            
                            echo"<form action='deleteSpecificDay' method='post' id='$day->Name'>";
                            echo csrf_field();
                            echo "<input type='hidden' name='netId' value='$change->NetId'>";
                            echo "<input type='hidden' name='date' value='$change->Date'>";
                            echo"<td><button type='submit' value='Submit' class='btn btn-danger'>Delete this day</button></td>";
                            echo"</form>";
                            //echo"<td>"; echo date("g:i a",strtotime($date->startTime)) . " - " . date("g:i a", strtotime($date->endTime)) . " "; echo"</td>";
                            echo "</tr>";
    
                        }
                    }
    
                    ?>
            </table>
                </div>

            <h3>Add a Regular Scheduled Day</h3>
            <form action="addRegularDay" method='post' id="updateRegularSchedule">
                {{csrf_field()}}
                
                <input type='hidden' name='empFName' value='<?php echo $user_new[0]->NameFirst?>'>
                <input type='hidden' name='empLName' value='<?php echo $user_new[0]->NameLast?>'>
                <input type='hidden' name='netId' value='<?php echo $user_new[0]->NetId; ?>'>
                
                <table class="table">
                    <th>Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <tr>
                        <td>
                            <select name="date" class="form-control">
                                <?php 
                                for($i = 0; $i < 5; $i++)
                                {          
                                    $date = date("l", strtotime("Monday +$i days"));
                                    if($date == "Monday") $dateEntry = 0;
                                    if($date == "Tuesday") $dateEntry = 1;
                                    if($date == "Wednesday") $dateEntry = 2;
                                    if($date == "Thursday") $dateEntry = 3;
                                    if($date == "Friday") $dateEntry = 4;
                                    echo "<option value='$dateEntry'>$date</option>";
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <select name="sTime" class="form-control">
                            <?php
                                for($i = 0; $i < 53; $i++)
                                {
                                    $sTime = date("g:i a", strtotime('07:00') + 15*60*$i);
                                    $sTimeEntry = date("H:i", strtotime("$sTime"));
                                    echo "<option value='$sTimeEntry'>$sTime</option>";
                                }
                            ?>
                            </select>
                        </td>
                        <td>
                            <select name="eTime" class="form-control">
                            <?php
                                for($i = 0; $i < 53; $i++)
                                {
                                    $eTime = date("g:i a", strtotime('07:00') + 15*60*$i);
                                    $eTimeEntry = date("H:i", strtotime("$eTime"));
                                    echo "<option value='$eTimeEntry'>$eTime</option>";
                                }
                            ?>
                            </select>
                        </td>
                    </tr>
                    <tr> 
                        <td>
                            <button type='submit' value='Submit' class="btn btn-success">Save changes</button>
                        </td>
                        <td></td><td></td><!--extra table datas to complete the form look-->
                    </tr>
                </table>
            </form>

            <h3>Add a Specific date</h3>
            
            <form action="addSpecificDay" method='post' id="addSpecificDay">
                {{csrf_field()}}
                
                <input type='hidden' name='empFName' value='<?php echo $user_new[0]->NameFirst?>'>
                <input type='hidden' name='empLName' value='<?php echo $user_new[0]->NameLast?>'>
                <input type='hidden' name='netId' value='<?php echo $user_new[0]->NetId; ?>'>
                
                <table class="table">
                    <th>Date</th>
                    <th>Type</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <tr>
                        <td>
                            <select name="date" class="form-control">
                                <?php 
                                for($i = 0; $i < 100; $i++)
                                {          
                                    $date = date("D, F d", strtotime("+$i days"));
                                    $dateEntry = date("Y-m-d", strtotime("$date"));
                                    echo "<option value='$dateEntry'>$date</option>";
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <select name="type" class="form-control">
                                <option value="1">Time off</option>
                                <option value="2">Schedule change</option>
                            </select>
                        </td>
                        <td>
                            <select name="sTime" class="form-control">
                            <?php
                                for($i = 0; $i < 53; $i++)
                                {
                                    $sTime = date("g:i a", strtotime('07:00') + 15*60*$i);
                                    $sTimeEntry = date("H:i", strtotime("$sTime"));
                                    echo "<option value='$sTimeEntry'>$sTime</option>";
                                }
                            ?>
                            </select>
                        </td>
                        <td>
                            <select name="eTime" class="form-control">
                            <?php
                                for($i = 0; $i < 53; $i++)
                                {
                                    $eTime = date("g:i a", strtotime('07:00') + 15*60*$i);
                                    $eTimeEntry = date("H:i", strtotime("$eTime"));
                                    echo "<option value='$eTimeEntry'>$eTime</option>";
                                }
                            ?>
                            </select>
                        </td>
                    </tr>
                    <tr> 
                        <td>
                            <button type='submit' value='Submit' class="btn btn-success">Save changes</button>
                        </td>
                        <td></td><td></td><!--extra table datas to complete the form look-->
                    </tr>
                </table>
            </form>
            

          @component('components.footer')
          @endcomponent
        </div>

      </div>

    </body>
</html>
