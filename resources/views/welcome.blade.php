<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
    <head>
        <meta charset="utf-8">

        <title>SFD Schedule</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="sfd-favicon.ico" />
    </head>
    <body>
      <div>

        <div class="container-fluid teamSchedule" style="background-color:white;">
        @component('components.topNav')
        @endcomponent

            <h1 align="center">Home</h1>
            
            <div style="border:1px solid; margin:5px;">
            <?php 
                $raw = file_get_contents('http://hsapp.hs.umt.edu/employee-database/2013_API/server.php/school/19/department/1/type/66,68/tag/72,127,138,528/JSON');
                
                $data = json_decode($raw);
                foreach($data as $person)
                {
                    if($person->FirstName === $user[0]->FirstName and $person->LastName === $user[0]->LastName)
                    {
                        $photoUrl = $person->Photo;
                        echo "<img src='http://hsapp.hs.umt.edu/employee-database/index.php/pubtools/serveFile/images/$photoUrl' style='max-height:75px; max-width:75px; padding:5px;' alt='Profile picture for $person->FirstName $person->LastName'";
                        echo "<br>";
                    }
                }
            
            ?>
                
            <h3>Regular Schedule for <?php echo $user[0]->FirstName . " " . $user[0]->LastName; ?></h3>
            
            <table>
                <tr>
                    <th>Day</th>
                    <th>Schedule</th>
                </tr>
                <?php 
                    if($days == NULL)
                    {
                        echo"<td>Nothing to show</td>";
                        echo"<td>Nothing to show</td>";
                    }
                    else
                    {
                        foreach($days as $day){
                        echo "<tr>";
                            echo"<td>"; echo $day->Name; echo"</td>";
                            //to show dates
                            // echo"<td>"; echo date("D, F d", strtotime("Last Monday +$count days")) . ": "; echo"</td>";
                            $emptyDay = true;
                            foreach($schedule as $shift){
                                if($days[$shift->DayCode]->Name == $day->Name) {
                                    if($emptyDay) echo "<td>";
                                    else echo " | ";
                                    echo date("g:i a",strtotime($shift->Start)) . " - " . date("g:i a",strtotime($shift->End)) . " ";
                                    $emptyDay = false;
                                }
                            }
                            if($emptyDay) {
                                echo"<td>"; echo "Not Scheduled"; echo"</td>";
                            }
                            else {
                                echo"</td>";
                            }
                            //echo"<td>"; echo date("g:i a",strtotime($date->startTime)) . " - " . date("g:i a", strtotime($date->endTime)) . " "; echo"</td>";
                        echo "</tr>";
    
                        }
                    }
    
                    ?>
            </table>
                
        </div>

        <div class="container-fluid teamSchedule" style="margin-top:10px; background-color:white; padding-left: 0px; padding-right: 0px; border:1px solid; margin:5px;">
            <h3>Schedule changes for specific days</h3>
            
                <table>
                    <tr>
                        <th>Day</th>
                        <th>Schedule</th>
                    </tr>
                    <?php 
                        if($days == NULL)
                        {
                            echo"<td>Nothing to show</td>";
                            echo"<td>Nothing to show</td>";
                        }
                        else
                        {
                            foreach($changes as $change){
                                echo "<tr>";
                                echo"<td>"; echo date("D, F d", strtotime($change->Date)); echo"</td>";
                                if($change->NetId == $user_new[0]->NetId) {
                                    echo "<td>";
                                    if($change->Type == 2) {
                                        echo date("g:i a",strtotime($change->Start)) . " - " . date("g:i a",strtotime($change->End)) . " ";
                                    }
                                    else {
                                        echo "Not Scheduled";
                                    }
                                    echo "</td>";
                                }

                                $netId = $user_new[0]->NetId;
                                echo "</tr>";
    
                            }
                        }
    
                    ?>
            </table>
        </div>

        @component('components.alert')
            @slot('type')
                info
                <!-- put type here, succes, failure -->
            @endslot
            
            @slot('title')
                <!-- put message here -->
                <h2 style="margin-top: 0px;">Notice</h2>
                <ul>
                    <li>All pages except "Daily Coverage" now use the regular schedule system.</li>
                    <li>Dates on pages are now calculated from the regular schedule and current week.</li>
                    <li>You can now change specific dates for schedule changes or time off.</li>
                    <li>Schedule changes are used when viewing Team schedules</li>
            @endslot
          @endcomponent

          @component('components.alert')
            @slot('type')
                danger
                <!-- put type here, succes, failure -->
            @endslot
            
            @slot('title')
                <!-- put message here -->
                <h2 style="margin-top: 0px;">Bug Fixes</h2>
                <ul>
                    <li>The update schedule page was showing schedule changes for every user, this has been resolved.</li>
                </ul>
            @endslot
          @endcomponent

          @component('components.footer')
          @endcomponent
        </div>

      </div>

    </body>
</html>
