<div class="alert alert-{{ $type }}" role="alert">
  {{ $title }}
</div>
