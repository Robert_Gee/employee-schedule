<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
    <head>
        <meta charset="utf-8">

        <title>SFD Schedule</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="sfd-favicon.ico" />
    </head>
    <body>
      <div>

        <div class="container-fluid teamSchedule" style="background-color:white;">
        @component('components.topNav')
        @endcomponent
          <h1 align="center">Team Schedule</h1>
            
            
            <?php
                foreach($users as $user){
                    echo "<div style='border:1px solid; margin:10px;'>";
                    $raw = file_get_contents('http://hsapp.hs.umt.edu/employee-database/2013_API/server.php/school/19/department/1/type/66,68/tag/72,127,138,528/JSON');
                
                    $data = json_decode($raw);
                    foreach($data as $person)
                    {
                        if($person->FirstName === $user->NameFirst and $person->LastName === $user->NameLast)
                        {
                            $photoUrl = $person->Photo;
                            echo "<img src='http://hsapp.hs.umt.edu/employee-database/index.php/pubtools/serveFile/images/$photoUrl' style='max-height:75px; max-width:75px; padding:5px; border:1px' alt='Profile picture for $person->FirstName $person->LastName'";
                            echo "<br>";
                        }
                    }
                    echo "<h3>Schedule for $user->NameFirst $user->NameLast</h3>";
                    echo "<table>";
                    echo "<th>Date</th>";
                    echo "<th>Schedule</th>";
                    $count = 0;
                    foreach($days as $day){
                        echo "<tr>";
                            //echo"<td>"; echo $day->Name; echo"</td>";
                            //to show dates
                            $today = date("l");
                            if($today == "Monday") {
                                echo"<td>"; echo date("D, F d", strtotime("Monday +$count days")) . ": "; echo"</td>";
                                $date = date("D, F d", strtotime("Monday +$count days"));
                            }
                            else {
                                echo"<td>"; echo date("D, F d", strtotime("Last Monday +$count days")) . ": "; echo"</td>";
                                $date = date("D, F d", strtotime("Last Monday +$count days"));
                            }
                            $count += 1;
                            $scheduleChange = false;

                            foreach($changes as $change) {
                                $changeDate = date("D, F d",strtotime($change->Date));
                                if($change->NetId == $user->NetId && $changeDate == $date) {
                                    if($scheduleChange == false) echo "<td>";
                                    else echo " | ";
                                    $scheduleChange = True;
                                    if($change->Type == 2) {
                                        echo date("g:i a",strtotime($change->Start)) . " - " . date("g:i a",strtotime($change->End)) . " ";
                                    }
                                    else {
                                        echo "Not Scheduled";
                                    }
                                    
                                }
                                if($scheduleChange == true) echo "</td>";
                            }

                            if($scheduleChange == false) {
                                $emptyDay = true;
                                foreach($shifts as $shift){
                                    if($days[$shift->DayCode]->Name == $day->Name && $shift->NetId == $user->NetId) {
                                        if($emptyDay) echo "<td>";
                                        else echo " | ";
                                        echo date("g:i a",strtotime($shift->Start)) . " - " . date("g:i a",strtotime($shift->End)) . " ";
                                        $emptyDay = false;
                                    }
                                }
                                if($emptyDay) {
                                    echo"<td>"; echo "Not Scheduled"; echo"</td>";
                                }
                                else {
                                    echo"</td>";
                                }
                            }
                            //echo"<td>"; echo date("g:i a",strtotime($date->startTime)) . " - " . date("g:i a", strtotime($date->endTime)) . " "; echo"</td>";
                        echo "</tr>";
    
                        }
                    echo "</table>";
                    echo "</div>";
                }
            ?>

          @component('components.footer')
          @endcomponent
        </div>

      </div>

    </body>
</html>
